<?php

$_SERVER['HTTPS'] = 'on';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LocalSettings
 * myIP: 95.105.179.163
 * pilot: pilot.shirtplatform.com
 * @author Janci
 */
class LocalSettings
{

    const IS_LIVE = false;
    //const SHOW_ERROR = true;
    //const FORCED_EDITOR_DOMAIN = 'smikolaj1.shirtplatform.com';

    /**
     * Relative path to yii framework
     */
    const relativePathToYii = '/../yii/framework/yii.php';

    const toolRestUrl = 'https://docker.shirtplatform.com/production-tool/rest';
    
    /**
     * Url to webservices
     */ 
    //const TRANSLATE_API_KEY = '';
     
    const webservicesURL = 'https://docker.shirtplatform.com/webservices/rest/';
    /* 
      public static $redisServer = array(
      'host' => '127.0.0.1',
      'port' => 6379,
      ); */
    
    const UNI_PASS = 'pass';

    public static $TRACE_WS = false;

    const AJAX_WS_DEBUG = false;

    //const AJAX_WS_DEBUG_DUPLICITIY_ONLY = true;

    public static $ENABLED_GOOGLETRANSLATE_ACCOUNTS = array('1');
    public static $TRANSLATIONS_DATABASE = array(
        'connectionString' => 'mysql:host=mysql-php;port=3306;dbname=demo',
        'username' => 'demo',
        'password' => 'demo',
#        'connectionString' => 'mysql:host=195.154.83.188;port=3306;dbname=dev_platform_editor',
#        'username' => 'developer',
#        'password' => 'developer2013',

    );

    /**
     * Default time zone
     */

    const DEFAULT_TIME_ZONE = 'Europe/Berlin';
    const DEFAULT_LOCALE = 'de_de';

    /**
     * Define log file according this manual: http://www.yiiframework.com/doc/guide/1.1/en/topics.logging
     * @var type 
     */
    public static $logConfig = array(
        'class' => 'CLogRouter',
        'routes' => array(
            /* array(
              'class'=>'CWebLogRoute',
              'levels'=>'error, warning, info , trace',
              //'categories'=>'*',
              'except'=>'system.CModule.*, application ',
              ), */
            array(
                'class' => 'CFileLogRoute',
                'levels' => 'error',
                'except' => 'system.CModule.*, application ',
                'maxLogFiles' => 1,
                'maxFileSize' => 10000,
                'logFile' => 'error.log',
            ),
            array(
                'class' => 'CFileLogRoute',
                'levels' => 'warning',
                'except' => 'system.CModule.*, application ',
                'maxLogFiles' => 1,
                'maxFileSize' => 10000,
                'logFile' => 'warning.log',
            ),
            array(
                'class' => 'CFileLogRoute',
                'levels' => 'info',
                'except' => 'system.CModule.*, application ',
                'maxLogFiles' => 1,
                'maxFileSize' => 10000,
                'logFile' => 'info.log',
            ),
            array(
                'class' => 'CFileLogRoute',
                'levels' => 'stackTrace',
                'except' => 'system.CModule.*, application ',
                'maxLogFiles' => 1,
                'maxFileSize' => 10000,
                'logFile' => 'error_trace.log',
            ),
            /*array(
                'class' => 'CFileLogRoute',
                'levels' => 'trace',
                'except' => 'system.CModule.*, application ',
                'maxLogFiles' => 1,
                'maxFileSize' => 10000,
                'logFile' => 'trace.log',
            ),*/
        ),
    );

    /**
     * Enabled tools to faciliate developing editor
     * like:
     * autologing after session expired on WS server
     * Debug logs shown at the end of the page
     */

    const DEVELOPER_MODE = true;

#    const cache = [
#        'class'=>'system.caching.CMemCache',
#        'servers'=>[
#	       ['host'=>'tcp://127.0.0.1', 'port'=>11211, 'weight'=>60],
#        ],
#    ];
#    
#    const session = [
#        'class' => 'CCacheHttpSession',
#        'timeout' => 6000,
#    ];
}

